/*
 * populationGen.cpp
 *
 *  Created on: Mar 9, 2017
 *      Author: lorin
 */

#include "populationGen.h"
#include <list>
#include <iostream>
#include <vector>
#include <sstream>
#include <string>
#include <stdio.h>
#include <math.h>
#include "../tinyxml/tinyxml.h"


Person::Person(int household, int ageIn, int work, int school, int CommunityIDin ){
	this->age = ageIn;
	this->WorkID = work;
	this->SchoolID = school;
	this->HouseholdID = household;
	this->CommunityID = CommunityIDin;
};


void Person::WriteToFile(std::ofstream* filenameout){

	std::stringstream converter;
	converter << std::endl << this->age << "," << this->HouseholdID << "," << this->SchoolID << "," << this->WorkID
	 << "," << this->CommunityID << "," << this->CommunityID ;
	std::string printstring;
	printstring = converter.str();
	*filenameout << printstring;

};

alias::alias(){
	size = 0;
	Alias.push_back(0);
	Original.push_back(0);
	Prob.push_back(0);
}

alias::alias(std::vector<float> probabilities, std::vector<unsigned int> ids){
	for(int i = 0; i < probabilities.size(); i++){
		probabilities[i] = probabilities[i] * (probabilities.size()-1);
		//std::cout << "prob: " << probabilities[i] << ", id: " << ids[i] << std::endl;
	}
	std::vector<float> T;
	std::vector<float>::iterator it = T.begin();
	//std::cout << "Alias call" << std::endl;
	std::vector<int> AliasTemp;
	std::vector<int>::iterator it2 = AliasTemp.begin();
	int n = probabilities.size();

	std::vector<float>::iterator tempit = it;
	std::vector<int>::iterator tempit2 = it2;
	for(int j = 0; j < n; j++){
		//std::cout << "inforalias" << std::endl;
		//std::cout << probabilities[j] << " " << ids[j] << std::endl;
		if(T.size()==0){
			T.push_back(probabilities[j]);
			AliasTemp.push_back(ids[j]);
		}
		else{
			for(int i = 0; i < T.size(); i++){
				tempit++;
				tempit2++;
				//std::cout<< "infor2"<< std::endl;
				if(T[i] <= probabilities[j] ){
					if(T.size()> i+1){
						if(T[i+1] > probabilities[j]){
							//std::cout << "endif?" << std::endl;
							T.insert(T.begin()+i+1 ,probabilities[j]);
							AliasTemp.insert(AliasTemp.begin() + i +1,ids[j]);
							//std::cout << "end if: " << i << " , " << std::endl;
							break;
						}
					}
					else{
						//std::cout << "endif?" << std::endl;
						T.push_back(probabilities[j]);
						AliasTemp.push_back(ids[j]);
						//std::cout << "end if: " << i << " , " << std::endl;
						break;
					}
				}
				else if(i == (T.size() -1)){
					//std::cout << "orelse?" << std::endl;
					T.push_back(probabilities[j]);
					AliasTemp.insert(AliasTemp.begin(), ids[j]);
					break;
				}
			}
		}
		//std::cout << "endforalias" << std::endl;

	}
	for(int i = 0; i < T.size();i++){
	//std::cout << T[i] << std::endl;

	}
	for(int j = 0; j < n-1; j++){
		//std::cout << "infor2" << std::endl;
		float pl = T.front();
		T.erase(T.begin());
		//std::cout << pl << std::endl;
		float pg = T.back();
		T.pop_back();
		int g = AliasTemp.back();
		AliasTemp.pop_back();
		int tag =AliasTemp.front();
		AliasTemp.erase(AliasTemp.begin());
		//std::cout << "erasesdone" << std::endl;
		/*if(pl <= 1)*/
			Prob.push_back(pl);
		/*else{
			Prob.push_back(1);
		}*/
		Original.push_back(tag);
		Alias.push_back(g);
		pg = pg - (1 - pl);
		tempit = T.begin();
		tempit2 = AliasTemp.begin();
		if(pg != 0){
			for(int i = 0; i < T.size(); i++){
				tempit++;
				tempit2++;
				if(T[i] < pg){
					T.insert(T.begin()+i+1, pg);
					AliasTemp.insert(AliasTemp.begin()+i+1, g);
					break;
				}
				else if(i == (T.size() -1)){
					T.push_back(pg);
					AliasTemp.push_back(g);
					break;
				}
			}
		}
	}
	for(int i = 0; i < Prob.size(); i++){
		//std::cout << "original: " << Original[i] << ", Prob: " << Prob[i] << ", Alias: " << Alias[i] << std::endl;
	}
	size = Prob.size();
	//std::cout << "alias generated, prob: " << Prob.size() << "Original: " << Original.size() << "Alias: " << Alias.size() << std::endl;

};

int alias::generate(float randomN, float aliasRandom){
	//std::cout << "generate call" << std::endl;

	int n = (int) (size * (rand() / (RAND_MAX + 1.0)));
	//std::cout << "testn: " << n << std::endl;
	int aliasID = Original[n];
	//std::cout << int(aliasRandom)%100 << " " <<Prob[n]*100 << std::endl;
	if(int(aliasRandom)%100 >= Prob[n]*100){
		aliasID = Alias[n];
	}
	//std::cout << aliasID << std::endl;
	return aliasID;
};


householdParser::householdParser(std::string filename){
	std::ifstream infile(filename);
    if (!infile.is_open())
    {
        std::exit(EXIT_FAILURE);
    }
	//std::cout << "parses" << std::endl;

	std::string na = "NA";
	std::string line;
	getline(infile, line, '\r');
	while (getline(infile, line, '\r'))
	{
		std::istringstream iss(line);
		std::string Age;
		//std::cout<< "inwhile" << std::endl;

		std::vector<int> record;

		while(true){
			getline(iss, Age, ',');
			if (Age == na){
				break;
			}
			//std::cout << "age: " <<Age << std::endl;
			record.push_back(std::stoi(Age, nullptr));
		}


		Households.push_back( record );
	}
};


std::vector<int> householdParser::getHousehold(int seed){
	int HouseholdNumber = seed % Households.size();
	return Households[HouseholdNumber];
};

bool inRadius(float radiusKm, float CoordtoMeters, float longitudeHouse, float latitudeHouse, float longitude, float latitude, bool coords = false){
	//std::cout << " coords:  " << coords << std::endl;
	if(coords){
		int earthRadiusKm = 6371;
		/*degrees * Math.PI / 180;*/
		float dLat = (latitudeHouse-latitude)* M_PI / 180;
		float dLon = (longitudeHouse-latitude)* M_PI / 180;

		latitude = latitude* M_PI / 180;
		latitudeHouse = latitudeHouse* M_PI / 180;

		float a = sin(dLat/2) * sin(dLat/2) +
			  sin(dLon/2) * sin(dLon/2) * cos(latitude) * cos(latitudeHouse);
		float c = 2 * atan2(sqrt(a), sqrt(1-a));
		float d = earthRadiusKm * c;
		//std::cout << " distance: " << d << ", radius: " << radiusKm << std::endl;
		if(d <= radiusKm){
		  return true;
		}
		return false;

	}
		else{
		float radius = radiusKm;
		float distanceLong = (longitudeHouse - longitude) *CoordtoMeters ;
		float distanceLat = (latitudeHouse - latitude) * CoordtoMeters;
		float distanceSq = distanceLong*distanceLong + distanceLat*distanceLat;
		if(distanceSq <= radius*radius)
			return true;
		return false;
	}
}

geoParser::geoParser(std::string fileName){
	coords = false;
	pop_Size = 500000;
	TiXmlDocument doc(fileName.c_str());
	bool loadokay = doc.LoadFile();
	if(!loadokay)
	{

		std::cerr << doc.ErrorDesc() << std::endl;
		exit(-1);
		return;
	}

	TiXmlElement* GeoverdelingsProfiel = doc.FirstChildElement();
	if(GeoverdelingsProfiel == NULL)
	{
		std::cerr << "Failed to load file: No root element."
			 << std::endl;
		doc.Clear();
		exit(-1);
		return;
	}
	//std::cout << "gothere1" << std::endl;
	//TiXmlElement* _cities =  GeoverdelingsProfiel->FirstChildElement();
	//TiXmlElement* villagesInfo =  _cities->NextSiblingElement();
	//std::string elemName = _cities->Value();
	for(TiXmlElement* part = GeoverdelingsProfiel->FirstChildElement(); part != NULL; part = part->NextSiblingElement()){
		std::string partName = part->Value();
		if(partName == "Cities"){
			for(TiXmlElement* _city = part->FirstChildElement(); _city != NULL; _city = _city->NextSiblingElement())
			{
				city* toPush = new city;
				for(TiXmlElement* elem = _city->FirstChildElement(); elem != NULL; elem = elem->NextSiblingElement())
				{
					std::string elemName = elem->Value();
					const char* attr;
					if(elemName == "ID")
					{
						attr = elem->Attribute("attribute1");
						if(attr != NULL){
							toPush->id =atoi(attr);
						}

					}
					else if(elemName == "longitude")
					{
						attr = elem->Attribute("attribute1");
						if(attr != NULL){
							toPush->longitude =std::stod(attr, NULL);
						}

					}
					else if(elemName == "latitude")
					{
						attr = elem->Attribute("attribute1");
						if(attr != NULL){
							toPush->latitude =std::stod(attr, NULL);
						}

					}
					else if(elemName == "size")
					{
						attr = elem->Attribute("attribute1");
						if(attr != NULL){
							toPush->size =atoi(attr);
						}

					}
				}
				cities.push_back(toPush);
			}
		}
		else if( partName == "Villages"){
			for(TiXmlElement* VillageData = part->FirstChildElement(); VillageData != NULL; VillageData = VillageData->NextSiblingElement()){
				std::string elemName = VillageData->Value();
				const char* attr;
				if(elemName == "totalNumber")
				{
					attr = VillageData->Attribute("attribute1");
					if(attr != NULL){
						nrVillages =atoi(attr);
					}

				}
				else if(elemName == "minLongitude")
				{
					attr = VillageData->Attribute("attribute1");
					if(attr != NULL){
						minLongitude =strtof(attr, NULL);
					}

				}
				else if(elemName == "maxLongitude")
				{
					attr = VillageData->Attribute("attribute1");
					if(attr != NULL){
						maxLongitude =strtof(attr, NULL);
					}

				}
				else if(elemName == "minLatitude")
				{
					attr = VillageData->Attribute("attribute1");
					if(attr != NULL){
						minLatitude =strtof(attr, NULL);
					}

				}
				else if(elemName == "maxLatitude")
				{
					attr = VillageData->Attribute("attribute1");
					if(attr != NULL){
						maxLatitude =strtof(attr, NULL);
					}

				}

				else{
					break;
				}
			}

		}

		else if(partName == "Settings"){
			for(TiXmlElement* VillageData = part->FirstChildElement(); VillageData != NULL; VillageData = VillageData->NextSiblingElement())
				{
				std::string elemName = VillageData->Value();
				const char* attr;
				if(elemName == "populationSize")
				{
					attr = VillageData->Attribute("attribute1");
					if(attr != NULL){
						pop_Size =strtof(attr, NULL);
					}

				}
				else if(elemName == "coordinateSystem")
				{
					attr = VillageData->Attribute("attribute1");
					if(attr != NULL){
						if(attr == "Realistic"){
							coords = true;
						}
						else{
							coords = false;
						}
					}

				}
			}
		}
	}
	//std::cout << "gothere2" << std::endl;

	//std::cout<< "gothere3" <<std::endl;

	//std::cout << "gothere4" << std::endl;
};


	PopulationGenerator::PopulationGenerator(std::string configfile = "src/GeoverdelingsProfiel.xml"){
		randGen = NULL;

		srand(time(NULL));
		CoordToMeters = 78.71;


		/*

		 *vanuit GeoVerdelingsProfiel
		*/
		int nrVillages = 10;
		float maxLatitude = 100;
		float minLatitude = 0;
		float maxLongitude = 100;
		float minLongitude = 0;
		geoParser geoParser(configfile);
		maxLatitude = geoParser.maxLatitude;
		minLatitude = geoParser.minLatitude;
		maxLongitude = geoParser.maxLongitude;
		minLongitude = geoParser.minLongitude;
		nrVillages = geoParser.nrVillages;
		bool coords = true;


		cities = geoParser.cities;
		popSize =  geoParser.pop_Size;


		//std::cout << "villagesdone" << std::endl;
		int communitySize = 2000;
		int nrCommunities = (popSize/communitySize) + 1;
		for(int i = 0; i < nrCommunities; i++){
			///*
			float ComLatitude = rand() % int((maxLatitude-minLatitude)*10000) + int(minLatitude*10000);
			float ComLongitude = rand() % int((maxLongitude-minLongitude)*10000) + int(minLongitude*10000);
			 //*/
			//float firstCheck = popSize - totalcitypop;

			community* TempCom = new community;
			TempCom->id = i;
			TempCom->latitude = ComLatitude/10000.0;
			TempCom->longitude = ComLongitude/10000.0;
			TempCom->size = 0;
			TempCom->maxSize = communitySize;
			communities.push_back(TempCom);


		}
		//std::cout << "communitiessdone" << std::endl;
		int schoolSize = 500;
		int nrSchools = (popSize/4)/schoolSize;
		for(int i = 0; i < nrSchools; i++){
			//assign ID, latitude, longitude and size
			//push into vector

			float SchoolLatitude = rand() + int((maxLatitude-minLatitude)*10000) + int(minLatitude*10000);
			float SchoolLongitude = rand() + int((maxLongitude-minLongitude)*10000) + int(minLongitude*10000);

			school* TempSchool = new school;
			TempSchool->id = i+1;
			TempSchool->latitude = SchoolLatitude/10000.0;
			TempSchool->longitude = SchoolLongitude/10000.0;
			TempSchool->size = 0;
			schools.push_back(TempSchool);

		}
		//std::cout << nrSchools << std::endl;
		//std::cout << schools[0]->id << std::endl;
		//std::cout<< schools[3]->latitude << std::endl;
		//std::cout << "schoolsdone" << std::endl;
		int FirstCollegeId = schools.size();
		int collegeSize = 3000;
		int nrColleges = (popSize)/collegeSize;
		for(int i = 0; i < nrColleges; i++){
			int chosenCityID = AliasmethodCities.generate(rand(), rand());
			//std::cout<< "generated " << chosenCityID <<  std::endl;
			college* tempCollege = new college;
			tempCollege->cityId = chosenCityID;
			tempCollege->size = 0;
			tempCollege->id = FirstCollegeId + i;
			tempCollege->maxSize = 3000;
			tempCollege->longitude = cities[chosenCityID]->longitude;
			tempCollege->latitude = cities[chosenCityID]->latitude;
			colleges.push_back(tempCollege);

		}
		//std::cout << "collegesdone " << nrColleges <<  std::endl;
		//std::cout<< colleges[0]->id << std::endl;
		//std::cout << colleges[0]->cityId << std::endl;
		//std::cout << colleges[1]->id << " " << colleges[0]->cityId << std::endl;
		int workplaceSize = 20;
		int nrWorkplaces = (popSize- popSize/3)/workplaceSize;
		for(int i = 0; i < nrWorkplaces; i++){

			float WorkLatitude = rand() + int((maxLongitude-minLongitude)*10000) + int(minLongitude*10000);;
			float WorkLongitude = rand() + int((maxLongitude-minLongitude)*10000) + int(minLongitude*10000);;

			workplace* TempWork = new workplace;
			TempWork->id = i;
			TempWork->latitude = WorkLatitude/10000.0;
			TempWork->longitude = WorkLongitude/10000.0;
			TempWork->size = 0;
			workplaces.push_back(TempWork);
			//assign ID, latitude, longitude and size
			//push into vector
		}
		//std::cout << "workplacesdone" << std::endl;

		std::vector<float> probability;
		std::vector<unsigned int> ids;
		totalcitypop = 0;
		for(unsigned int k = 0; k < cities.size(); k++){
			ids.push_back(cities[k]->id);
			totalcitypop += cities[k]->size;
		}
		for(unsigned int k = 0; k < cities.size(); k++){

			probability.push_back((cities[k]->size)/totalcitypop);
		}
		AliasmethodCities = alias(probability, ids);

		for(int i = 0; i < nrVillages; i++){

			float VilLatitude = rand() % int((maxLongitude-minLongitude)*100000) + int(minLongitude*100000);
			float VilLongitude = rand() % int((maxLongitude-minLongitude)*100000) + int(minLongitude*100000);
			//int templat = VilLatitude;
			//int templong = VilLongitude;
			//std::cout << " coords: "<<VilLatitude/10000.0 << " " << VilLongitude/10000.0 << std::endl;
			village* TempVil = new village;
			TempVil->id = i;
			TempVil->latitude = VilLatitude/100000.0;
			TempVil->longitude = VilLongitude/100000.0;
			TempVil->size = 0;
			/************************************************/
			/******************Colleges**********************/
			/************************************************/
			float SearchRange = 5;
			while(TempVil->chosenColleges.size() < 1 ){
				//std::cout << "stuck in college... tell me about it" << std::endl;
				for (unsigned int i = 0; i < colleges.size(); i++){
					if(inRadius(SearchRange, CoordToMeters, TempVil->longitude, TempVil->latitude ,colleges[i]->longitude, colleges[i]->latitude, coords)){
						TempVil->chosenColleges.push_back(colleges[i]);
					}
				}
				SearchRange = SearchRange*2;
			}

			/************************************************/
			/*******************Schools**********************/
			/************************************************/
			SearchRange = 5;
			while(TempVil->chosenSchools.size() < 1 ){
				//std::cout << "stuck in school" << std::endl;
				for (unsigned int i = 0; i < schools.size(); i++){
					if(inRadius(SearchRange, CoordToMeters, TempVil->longitude, TempVil->latitude ,schools[i]->longitude, schools[i]->latitude,coords)){
						TempVil->chosenSchools.push_back(schools[i]);
					}
				}
				SearchRange = SearchRange*2;
			}

			/************************************************/
			/*******************Workplaces*******************/
			/************************************************/
			SearchRange = 5;
			//std::cout << "whilecheckpoint1" << std::endl;
			while(TempVil->chosenWorkplaces.size() < 1 ){
				//std::cout << "stuck at work" << std::endl;
				for (unsigned int i = 0; i < workplaces.size(); i++){
					if(inRadius(SearchRange, CoordToMeters, TempVil->longitude, TempVil->latitude ,workplaces[i]->longitude, workplaces[i]->latitude, coords)){
						TempVil->chosenWorkplaces.push_back(workplaces[i]);
					}
				}
				SearchRange = SearchRange*2;
			}

			/************************************************/
			/******************Communities*******************/
			/************************************************/
			SearchRange = 5;
			std::vector< community* > tempcom = communities;
			while(tempcom.size() > 0 ){
				//std::cout << "stuck in community" << std::endl;
				//if(communities.size() == 0){std::cout << "trouble: " << nrCommunities << std::endl;}
				for (unsigned int i = 0; i < tempcom.size(); i++){
					if(inRadius(SearchRange, CoordToMeters, TempVil->longitude, TempVil->latitude ,tempcom[i]->longitude, tempcom[i]->latitude, coords)){
						TempVil->chosenCommunities.push_back(tempcom[i]);
						tempcom.erase(tempcom.begin() + i);
						i--;
					}

				}
				SearchRange = SearchRange*2;
			}
			villages.push_back(TempVil);

		}
		//std::cout << "passed villas" << std::endl;

		for(unsigned int i = 0; i < cities.size(); i++){

			//std::cout << cities.size() << std::endl;
			//cities[0]->chosenColleges.push_back(colleges[0]);
			//std::cout << colleges.size() << std::endl;
			/************************************************/
			/******************Colleges**********************/
			/************************************************/
			float SearchRange = 5;
			while(cities[i]->chosenColleges.size() < 1 ){

				//std::cout << "stuck in college... tell me about it" << std::endl;
				for (unsigned int j = 0; j < colleges.size(); j++){
					//std::cout << "wtf??" << std::endl;
					if(inRadius(SearchRange, CoordToMeters, cities[i]->longitude, cities[i]->latitude ,colleges[j]->longitude, colleges[j]->latitude, coords)){
						cities[i]->chosenColleges.push_back(colleges[j]);
					}
				}
				SearchRange = SearchRange*2;
				//td::cout << SearchRange << std::endl;
			}
			//std::cout << "passed city college" << std::endl;
			/************************************************/
			/*******************Schools**********************/
			/************************************************/
			SearchRange = 5;
			while(cities[i]->chosenSchools.size() < 1 ){
				//std::cout << "stuck in school" << std::endl;
				for (unsigned int j = 0; j < schools.size(); j++){
					if(inRadius(SearchRange, CoordToMeters, cities[i]->longitude, cities[i]->latitude ,schools[j]->longitude, schools[j]->latitude, coords)){
						cities[i]->chosenSchools.push_back(schools[j]);
					}
				}
				SearchRange = SearchRange*2;
			}
			//std::cout << "passed city school" << std::endl;
			/************************************************/
			/*******************Workplaces*******************/
			/************************************************/
			SearchRange = 5;
			//std::cout << "whilecheckpoint1" << std::endl;
			while(cities[i]->chosenWorkplaces.size() < 1 ){
				//std::cout << "stuck at work" << std::endl;
				for (unsigned int j = 0; j < workplaces.size(); j++){
					if(inRadius(SearchRange, CoordToMeters, cities[i]->longitude, cities[i]->latitude ,workplaces[j]->longitude, workplaces[j]->latitude, coords)){
						cities[i]->chosenWorkplaces.push_back(workplaces[j]);
					}
				}
				SearchRange = SearchRange*2;
			}
			//std::cout << "passed city work" << std::endl;
			/************************************************/
			/******************Communities*******************/
			/************************************************/
			SearchRange = 5;
			std::vector< community* > tempcom = communities;
			//std::cout << "pushing comms \n";
			while(tempcom.size() > 0 ){
				//std::cout << "Timesin While for: " << cities[i]->id  << std::endl;
				//std::cout << "stuck in community" << std::endl;
				//if(communities.size() == 0){std::cout << "trouble: " << nrCommunities << std::endl;}
				for (unsigned int j = 0; j < tempcom.size(); j++){
					if(inRadius(SearchRange, CoordToMeters, cities[i]->longitude, cities[i]->latitude ,tempcom[j]->longitude, tempcom[j]->latitude, coords)){
						cities[i]->chosenCommunities.push_back(tempcom[j]);
						//std::cout << "comm id: " <<tempcom[j]->id << "comm long " << tempcom[j]->longitude << "comm lat" <<tempcom[j]->latitude << std::endl;
						tempcom.erase(tempcom.begin() + j);
						j--;
					}


				}
				SearchRange = SearchRange*2;
			}
			//std::cout << "first in line: " << cities[i]->chosenCommunities[0]->id << std::endl;
			//std::cout << "longitude: " << cities[i]->longitude << " ,latitude " << cities[i]->latitude << std::endl;
			//std::cout << "passed city commies" << std::endl;
		}
		//std::cout << "got passed the setup" << std::endl;


	};



	bool PopulationGenerator::generate(std::string filenameout){

			int totalpop = 0;
			std::ofstream householdFile;
			householdFile.open ("SynthPop_geo.csv");
			householdFile << "\"hh_id\",\"latitude\",\"longitude\",\"size\"";


			std::ofstream popFile;
			popFile.open ("SynthPop.csv");
			popFile <<	"\"age\",\"household_id\",\"school_id\",\"work_id\",\"primary_community\",\"secondary_community\"";

			householdParser households = householdParser("src/Households_SynthPop.csv");

			//std::cout << "householdparserdone" << std::endl;

			int HouseID = -1;
			int totalVillagePop = popSize - totalcitypop ;


			//make households until you're above the asked popsize
			//std::cout << "startingwhile" << std::endl;

			while(totalpop < popSize){
				//std::cout << "inwhile" << std::endl;
				int houseSize;
				HouseID ++;
				int houseSizeDraw = rand();
				std::vector<int> household = households.getHousehold(houseSizeDraw);
				houseSize = household.size();
				totalpop += houseSize;
				int locationSeed = rand() % popSize;
				float HouseLong;
				float HouseLat;
				bool village = false;
				int chosenVillageId = 0;
				int chosenCityId = 0;

				//std::cout << "locationseed: " <<locationSeed << " citypop: " << totalcitypop << " popsize: " << popSize <<std::endl;
				//std::cout<<cities.size()<<std::endl;
				if (locationSeed <= totalcitypop){
					chosenCityId = AliasmethodCities.generate(rand(), rand());
					//std::cout << cities[chosenCityId]->id<< std::endl;
					HouseLong = cities[chosenCityId]->longitude;
					HouseLat = cities[chosenCityId]->latitude;
					//cities[chosenCityId]->size++;
				}
				else{
					//std::cout << "inelse" << std::endl;
					chosenVillageId = rand() % villages.size();
					HouseLong = villages[chosenVillageId]->longitude;
					HouseLat = villages[chosenVillageId]->latitude;
					village = true;
					if(HouseLong > 100){
						//std::cout << "WTF??: long " << HouseLong << " lat: " << HouseLat << std::endl;
					}
				}

				//std:: cout << "cityId: "<< chosenCityId << std::endl;
				int communityID = 0;
				if(village){

					int chosenCommunity = 0;
					while(true){
						if(villages[chosenVillageId]->chosenCommunities[chosenCommunity]->size < villages[chosenVillageId]->chosenCommunities[chosenCommunity]->maxSize ){
							villages[chosenVillageId]->chosenCommunities[chosenCommunity]->size +=houseSize;
							communityID = villages[chosenVillageId]->chosenCommunities[chosenCommunity]->id;
							break;

						}
						chosenCommunity++;
						if(chosenCommunity > villages[chosenVillageId]->chosenCommunities.size()){
							std::cout << "Made too little communities, woops" << std::endl;
							break;
						}

					}
				}
				else{

					int chosenCommunity = 0;
					while(true){
						if(cities[chosenCityId]->chosenCommunities[chosenCommunity]->size < cities[chosenCityId]->chosenCommunities[chosenCommunity]->maxSize ){
							cities[chosenCityId]->chosenCommunities[chosenCommunity]->size +=houseSize;
							communityID = cities[chosenCityId]->chosenCommunities[chosenCommunity]->id;
							break;

						}
						chosenCommunity++;
						if(chosenCommunity > cities[chosenCityId]->chosenCommunities.size()){
							std::cout << "Made too little communities, woops" << std::endl;
							break;
						}

					}
				}
				std::vector<Person> PeopleInHouse;

				householdFile << std::endl << HouseID << "," << HouseLat << "," << HouseLong << "," << houseSize;

				//filling up with people
				for(int i = 0; i < houseSize; i++){
					int age = household[i];

					int workID = 0;
					int schoolID = 0;
					if(age <= 18){
						if(village){
							int chosenschool =rand() % villages[chosenVillageId]->chosenSchools.size();
							schoolID = villages[chosenVillageId]->chosenSchools[chosenschool]->id;
							villages[chosenVillageId]->chosenSchools[chosenschool]->size++;
						}
						else{
							int chosenschool =rand() % cities[chosenCityId]->chosenSchools.size();
							schoolID = cities[chosenCityId]->chosenSchools[chosenschool]->id;
							cities[chosenCityId]->chosenSchools[chosenschool]->size++;
						}
					}
					else if(age <= 26){
						int studies = rand() % 100;
						if(studies <= 25){
							if(village){
								int chosenCollege = rand () % villages[chosenVillageId]->chosenColleges.size();
								villages[chosenVillageId]->chosenColleges[chosenCollege]->size++;
								schoolID = villages[chosenVillageId]->chosenColleges[chosenCollege]->id;
							}
							else{
								int chosenCollege =rand() % cities[chosenCityId]->chosenColleges.size();
								schoolID = cities[chosenCityId]->chosenColleges[chosenCollege]->id;
								cities[chosenCityId]->chosenColleges[chosenCollege]->size++;
							}
						}
					}
					else if(age <= 66 && schoolID ==0){

						int Hasjob = rand() % 100;
						if(Hasjob < 91){
							if (village){
								int chosenworkplace = rand()% villages[chosenVillageId]->chosenWorkplaces.size();
								workID = villages[chosenVillageId]->chosenWorkplaces[chosenworkplace]->id;
								villages[chosenVillageId]->chosenWorkplaces[chosenworkplace]->size ++;
							}
							else{
								int chosenworkplace = rand()% cities[chosenCityId]->chosenWorkplaces.size();
								workID = cities[chosenCityId]->chosenWorkplaces[chosenworkplace]->id;
								cities[chosenCityId]->chosenWorkplaces[chosenworkplace]->size ++;
							}
						}
						else{
							workID = 0;
						}
					}



					int work = workID;
					int school = schoolID;
					int community = communityID;
					PeopleInHouse.push_back(Person(HouseID, age, work, school,  community ));

				}

				for(unsigned int i = 0; i < PeopleInHouse.size(); i++){
					PeopleInHouse[i].WriteToFile(&popFile);
				}


			}
			//std::cout << "done" << std::endl;
			popFile.close();
			householdFile.close();
			return true;

	}



