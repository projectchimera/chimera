################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Observer.cpp \
../src/Subject.cpp \
../src/VisParser.cpp \
../src/Visualisation.cpp \
../src/Window.cpp 

OBJS += \
./src/Observer.o \
./src/Subject.o \
./src/VisParser.o \
./src/Visualisation.o \
./src/Window.o 

CPP_DEPS += \
./src/Observer.d \
./src/Subject.d \
./src/VisParser.d \
./src/Visualisation.d \
./src/Window.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -std=c++0x -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


