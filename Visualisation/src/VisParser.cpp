/**
 * @file
 * Implementation of the VisParser class.
 */

#include "VisParser.h"
#include "tinyxml/tinyxml.h"
#include <iostream>

namespace Visualisation {

	VisParser& VisParser::getInstance() {
		static VisParser parser;
		return parser;
	}

	VisParser::VisParser() {
		// Parse the default.xml file
		// See TinyXml-documentation for more info
		TiXmlDocument doc("Resources/default.xml");
		bool loadokay = doc.LoadFile();
		if(!loadokay){
			std::cerr << doc.ErrorDesc() << std::endl;
			exit(-1);
			return;
		}

		TiXmlElement* VisDefaults = doc.FirstChildElement();
		if(VisDefaults == NULL) {
			std::cerr << "Failed to load file: No root element." << std::endl;
			doc.Clear();
			exit(-1);
			return;
		}

		for(TiXmlElement* part = VisDefaults->FirstChildElement(); part != NULL; part = part->NextSiblingElement()){
			std::string partName = part->Value();
			if(partName == "Window"){
				for(TiXmlElement* WindowData = part->FirstChildElement(); WindowData != NULL; WindowData = WindowData->NextSiblingElement()){
					std::string elemName = WindowData->Value();
					const char* attr;
					if(elemName == "Width"){
						attr = WindowData->Attribute("attribute1");
						if(attr != NULL){
							width = atoi(attr);
						}
					}
					else if(elemName == "Height"){
						attr = WindowData->Attribute("attribute1");
						if(attr != NULL){
							height = atoi(attr);
						}
					}
					else {
						break;
					}
				}

			}
			else if( partName == "Cells"){
				for(TiXmlElement* CellData = part->FirstChildElement(); CellData != NULL; CellData = CellData->NextSiblingElement()){
					std::string elemName = CellData->Value();
					const char* attr;
					if(elemName == "X"){
						attr = CellData->Attribute("attribute1");
						if(attr != NULL){
							x = atoi(attr);
						}
					}
					else if(elemName == "Y"){
						attr = CellData->Attribute("attribute1");
						if(attr != NULL){
							y = atoi(attr);
						}

					}
					else{
						break;
					}
				}
			}

			else if(partName == "File"){
				for(TiXmlElement* FileData = part->FirstChildElement(); FileData != NULL; FileData = FileData->NextSiblingElement()){
					std::string elemName = FileData->Value();
					const char* attr;
					if(elemName == "Name"){
						attr = FileData->Attribute("attribute1");
						if(attr != NULL){
							filename = std::string(attr);
						}
					}
				}
			}
			else if(partName == "Colors"){
				for(TiXmlElement* color = part->FirstChildElement(); color != NULL; color = color->NextSiblingElement()){
					std::string colorName = color->Value();
					if(colorName == "Textcolor"){
						int r = 0;
						int g = 0;
						int b = 0;
						for(TiXmlElement* ColorData = color->FirstChildElement(); ColorData != NULL; ColorData = ColorData->NextSiblingElement()){
							std::string elemName = ColorData->Value();
							const char* attr;
							if(elemName == "r"){
								attr = ColorData->Attribute("attribute1");
								if(attr != NULL){
									r = atoi(attr);
								}
							}
							else if(elemName == "g"){
								attr = ColorData->Attribute("attribute1");
								if(attr != NULL){
									g = atoi(attr);
								}
							}
							else if(elemName == "b"){
								attr = ColorData->Attribute("attribute1");
								if(attr != NULL){
									b = atoi(attr);
								}
							}
							else{
								break;
							}
						}
						textcolor = sf::Color(r,g,b);
					}
					else if(colorName == "Healthycolor"){
						int r = 0;
						int g = 0;
						int b = 0;
						for(TiXmlElement* ColorData = color->FirstChildElement(); ColorData != NULL; ColorData = ColorData->NextSiblingElement()){
							std::string elemName = ColorData->Value();
							const char* attr;
							if(elemName == "r"){
								attr = ColorData->Attribute("attribute1");
								if(attr != NULL){
									r = atoi(attr);
								}
							}
							else if(elemName == "g"){
								attr = ColorData->Attribute("attribute1");
								if(attr != NULL){
									g = atoi(attr);
								}
							}
							else if(elemName == "b"){
								attr = ColorData->Attribute("attribute1");
								if(attr != NULL){
									b = atoi(attr);
								}
							}
							else{
								break;
							}
						}
						healthycolor = sf::Color(r,g,b);
					}
					if(colorName == "Infectedcolor"){
						int r = 0;
						int g = 0;
						int b = 0;
						for(TiXmlElement* ColorData = color->FirstChildElement(); ColorData != NULL; ColorData = ColorData->NextSiblingElement()){
							std::string elemName = ColorData->Value();
							const char* attr;
							if(elemName == "r"){
								attr = ColorData->Attribute("attribute1");
								if(attr != NULL){
									r = atoi(attr);
								}
							}
							else if(elemName == "g"){
								attr = ColorData->Attribute("attribute1");
								if(attr != NULL){
									g = atoi(attr);
								}
							}
							else if(elemName == "b"){
								attr = ColorData->Attribute("attribute1");
								if(attr != NULL){
									b = atoi(attr);
								}
							}
							else{
								break;
							}
						}
						infectedcolor = sf::Color(r,g,b);
					}
				}
			}
		}
	}

} /* namespace Visualisation */
