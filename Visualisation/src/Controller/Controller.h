#ifndef CONTROLLER_H_
#define CONTROLLER_H_

/**
 * @file
 * Header for the Controller class.
 */

#include <SFML/Graphics.hpp>
#include "../Window.h"

namespace Visualisation {
	namespace Controller {

		class Controller {
		public:
			/// Default constructor
			Controller();
			/// Default destructor
			virtual ~Controller();

			/// Pure virtual run-method
			virtual void run() = 0;

		protected:
			sf::RenderWindow* window;				///< Pointer to window
		};

	} /* namespace Controller */
} /* namespace Visualisation */

#endif /* CONTROLLER_H_ */
