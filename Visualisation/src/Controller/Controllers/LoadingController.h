#ifndef LOADINGCONTROLLER_H_
#define LOADINGCONTROLLER_H_

/**
 * @file
 * Header for the LoadingController class.
 */

#include "../Controller.h"
#include "../../Model/Model.h"

namespace Visualisation {
	namespace Controller {

		class LoadingController: public Controller {
		public:
			/**
			 * Constructor for the LoadingController class
			 * @Param model			The model to be used
			 */
			LoadingController(Model::Model* model);
			/// Default destructor
			virtual ~LoadingController();

			/// Implementation of Controller::run()
			virtual void run();

		protected:
			/// Read the next step in the model
			void readStep();
			/// Draw the loading text to the window
			void draw();

			Model::Model* model;				///< Pointer to the model used
		};

	} /* namespace Controller */
} /* namespace Visualisation */

#endif /* LOADINGCONTROLLER_H_ */
