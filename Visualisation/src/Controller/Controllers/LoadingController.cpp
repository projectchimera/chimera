/**
 * @file
 * Implementation of the Controller class.
 */

#include "LoadingController.h"

#include "../../View/Views/LoadingView.h"

namespace Visualisation {
	namespace Controller {

		LoadingController::LoadingController(Model::Model* model) : Controller() {
			// Set the model
			this->model = model;
			// Create the view and set the model
			View::LoadingView* view = new View::LoadingView();
			view->setModel(model);

			// Run the loading screen
			run();

			// Run final model generation methods
			model->generateClusters();
			model->addHouseholds();
			model->removeEmptyClusters();

			// Cleanum
			model->removeObs(view);
			delete view;
		}

		LoadingController::~LoadingController() {

		}

		void LoadingController::draw() {
			model->Subject::notify();
		}

		void LoadingController::readStep(){
			std::string file = "Resources/LatestRun/stepfile";
			file += std::to_string(model->getCurrentStep());
			file += ".csv";
			model->readStepFile(file);
		}

		void LoadingController::run() {
			while(window->isOpen()){
				if (window->hasFocus()){
					sf::Event event;
					while (window->pollEvent(event)){
						switch (event.type) {
						case sf::Event::Closed: {
							window->close();
							break;
						}
						default: {
							break;
						}
						}
					}
					if (model->getCurrentStep() > model->getSteps()){
						break;
					}
					draw();
					readStep();
				}
			}
		}

	} /* namespace Controller */
} /* namespace Visualisation */
