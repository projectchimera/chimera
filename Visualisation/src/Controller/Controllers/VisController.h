#ifndef VISCONTROLLER_H_
#define VISCONTROLLER_H_

/**
 * @file
 * Header for the VisController class.
 */

#include "../Controller.h"
#include "../../Model/Model.h"

namespace Visualisation {
	namespace Controller {

		class VisController : public Controller {

		public:
			/**
			 * Constructor for the VisController
			 * @param x				Number of clusters on the x-axis
			 * @param y				Number of clusters on the y-axis
			 * @param file			_geo.csv file to use as base
			 */
			VisController(int x, int y, std::string file);
			/// Default destructor
			virtual ~VisController();

			/// Implementation of Controller::run()
			virtual void run();

		protected:
			/// Draws the clusters to the screen
			void draw();

			/// Draws the help screen
			void help() const;

			Model::Model* model;				///< Pointer to the model used

			int x;								///< Number of clusters on the x-axis
			int y;								///< Number of clusters on the y-axis

			int zoomlvl;						///< Zoomlvl of the visualisation
		};

	}
}

#endif /* VISCONTROLLER_H_ */
