#ifndef CONTROLLER_GRAPHCONTROLLER_H_
#define CONTROLLER_GRAPHCONTROLLER_H_

/**
 * @file
 * Header for the Controller class.
 */

#include "../Controller.h"
#include "../../Model/Cluster.h"

namespace Visualisation {
	namespace Controller {

		class GraphController : public Controller {
		public:
			/**
			 * Constructor for the graphcontroller
			 * @param c			Pointer to the cluster to be used
			 * @param size		The amount of steps in the visualisation
			 */
			GraphController(Model::Cluster* c, int size);
			/// Default destructor
			virtual ~GraphController();

			/// Implementation of Controller::run()
			virtual void run();

		protected:
			/// Draws the cluster to the screen
			void draw();

			/// Draws the help screen
			void help() const;

			Model::Cluster* cluster;			///< Pointer to the cluster used

			int size;							///< Amount of steps in the visualisation
		};

	} /* namespace Controller */
} /* namespace Visualisation */

#endif /* CONTROLLER_GRAPHCONTROLLER_H_ */
