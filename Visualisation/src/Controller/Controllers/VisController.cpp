/**
 * @file
 * Implementation of the Controller class.
 */

#include "VisController.h"

#include "../../View/Views/VisView.h"
#include "GraphController.h"
#include "LoadingController.h"
#include <TGUI/TGUI.hpp>

#include "../../VisParser.h"

namespace Visualisation {
	namespace Controller {

		VisController::VisController(int x, int y, std::string file) : Controller() {
			this->x = x;
			this->y = y;
			zoomlvl = 1;

			// Create the model
			this->model = new Model::Model(x,y,file);

			LoadingController* cont = new LoadingController(model);
			delete cont;

			// Create a view and set it's model
			View::VisView* view = new View::VisView();
			view->setModel(model);

			// Run the visualization
			run();

			// Cleanup
			delete model;
			delete view;
		}

		VisController::~VisController() {

		}

		void VisController::draw() {
			// If clusters are updated , notify the model
			std::vector<Model::Cluster*> clusters = model->getClusters();
			for (unsigned int i = 0; i < clusters.size(); i++){
				clusters[i]->notify();
			}
			// If model is updated , notify the model
			model->Subject::notify();
		}

		void VisController::run() {
			int step = 0;
			model->setInfected(0);
			while (window->isOpen()){
				if (window->hasFocus()){
					sf::Event event;
					while (window->pollEvent(event)){
						switch (event.type) {
						case sf::Event::Closed: {
							window->close();
							break;
						}
						case sf::Event::KeyReleased: {
							switch (event.key.code) {
							// If escape is pressed, close the window
							case sf::Keyboard::Escape: {
								window->close();
								break;
							}
							// If h is pressed, open the help-screen
							case sf::Keyboard::H: {
								help();
								break;
							}
							// If backspace is pressed, move back one step
							case sf::Keyboard::BackSpace: {
								if (step != 0){
									step--;
								}
								model->setInfected(step);
								break;
							}
							// If space is pressed , move forward one step
							case sf::Keyboard::Space: {
								if (step != model->getSteps()){
									step++;
								}
								model->setInfected(step);
								break;
							}
							// If + or p is pressed , zoom in on middle
							case sf::Keyboard::P: {

							}
							case sf::Keyboard::Add: {
								zoomlvl = zoomlvl * 2;
								sf::View currentView = window->getView();
								currentView.zoom(0.5f);
								window->setView(currentView);
								model->zoomChanged();
								break;
							}
							// If - or m is pressed , zoom out
							case sf::Keyboard::M: {

							}
							case sf::Keyboard::Subtract: {
								if (zoomlvl > 1){
									zoomlvl = zoomlvl / 2;
									if (zoomlvl == 1){
										sf::View currentView(sf::Vector2f(500,500),sf::Vector2f(1000,1000));
										window->setView(currentView);
									}
									else {
										sf::View currentView = window->getView();
										currentView.zoom(2.f);
										window->setView(currentView);
									}
									model->zoomChanged();
								}
								break;
							}
							// If left-arrow is pressed , move camera left
							case sf::Keyboard::Left: {
								sf::View currentView = window->getView();
								if (currentView.getCenter().x - currentView.getSize().x / 2 > 0) {
									currentView.move(-1000/zoomlvl/2,0);
									window->setView(currentView);
									model->zoomChanged();
								}
								break;
							}
							// If right-arrow is pressed , move camera right
							case sf::Keyboard::Right: {
								sf::View currentView = window->getView();
								if (currentView.getCenter().x + currentView.getSize().x / 2 < 1000) {
									currentView.move(1000/zoomlvl/2,0);
									window->setView(currentView);
									model->zoomChanged();
								}
								break;
							}
							// If up-arrow is pressed , move camera up
							case sf::Keyboard::Up: {
								sf::View currentView = window->getView();
								if (currentView.getCenter().y - currentView.getSize().y / 2 > 0) {
									currentView.move(0,-1000/zoomlvl/2);
									window->setView(currentView);
									model->zoomChanged();
								}
								break;
							}
							// If down-arrow is pressed , move camera down
							case sf::Keyboard::Down: {
								sf::View currentView = window->getView();
								if (currentView.getCenter().y + currentView.getSize().y / 2 < 1000) {
									currentView.move(0,1000/zoomlvl/2);
									window->setView(currentView);
									model->zoomChanged();
								}
								break;
							}
							default: {
								break;
							}
							}
							break;
						}
						case sf::Event::MouseButtonPressed:{
							// If left mouse button pressed , calculate current position and draw graph according to correct cluster
							if (event.mouseButton.button == sf::Mouse::Left){
								sf::Vector2i pixelPos = sf::Mouse::getPosition(*window);
								sf::Vector2f worldPos = window->mapPixelToCoords(pixelPos);
								int clusternumber = -1;
								std::vector<Model::Cluster*> clusters = model->getClusters();
								for (unsigned int i = 0; i < clusters.size(); i++){
									if (clusters[i]->getX() == int(worldPos.x/1000*x)){
										if (clusters[i]->getY() == int(worldPos.y/1000*y)) {
											clusternumber = i;
											break;
										}
									}
								}
								// If cluster is found, draw the graph for it
								if (clusternumber != -1) {
									sf::View currentView = window->getView();
									GraphController* graph = new GraphController(model->getClusters()[clusternumber],model->getSteps());
									delete graph;
									window->setView(currentView);
								}
							}
							break;
						}
						default: {
							break;
						}
						}
					}
					draw();
				}
			}
		}

		void VisController::help() const{
			VisParser* visparser = &VisParser::getInstance();

			// Create window
			sf::RenderWindow window(sf::VideoMode(400,800), "Help");
			window.setFramerateLimit(60);

			// Create help text
			sf::Font font;
			font.loadFromFile("Resources/ADayinSeptember.otf");
			sf::Text* text = new sf::Text();
			text->setString("Welcome to the help screen for the Visualization tool.\n\n"
					"Controls:\n"
					"* Main visualization:"
					"   Space ------------ load next step.\n"
					"   Backspace -------- load previous step.\n"
					"   + (or p) --------- zoom in.\n"
					"   - (or m) --------- zoom out.\n"
					"   Left arrow ------- move screen left.\n"
					"   Right arrow ------ move screen right.\n"
					"   Up arrow --------- move screen up.\n"
					"   Down arrow ------- move screen down.\n"
					"   Escape ----------- close visualization.\n"
					"   h ---------------- open this help screen.\n"
					"   Left-clicking on any cell will open\n"
					"   the graph for that cell.\n"
					"* Graphs:\n"
					"   h ---------------- open this help screen.\n"
					"   Left arrow ------- move timestap left.\n"
					"   Right arrow ------ move timestap right.\n"
					"   Escape ----------- return to main visualization.\n\n"
					"Pressing the escape button will close this window.");
			text->setFont(font);
			text->setColor(visparser->getTextcolor());
			text->setCharacterSize(23);
			sf::FloatRect textRect = text->getLocalBounds();
			text->setOrigin(textRect.left + textRect.width / 2.f, textRect.top + textRect.height / 2.f);
			text->setPosition(200,400);

			while (window.isOpen()) {
				sf::Event event;
				while (window.pollEvent(event)) {
					switch(event.type){
					case sf::Event::Closed: {
						window.close();
						break;
					}
					case sf::Event::KeyReleased: {
						switch (event.key.code) {
						// If escape is pressed, close the window
						case sf::Keyboard::Escape: {
							window.close();
							break;
						}
						default: {
							break;
						}
						}
						break;
					}
					default: {
						break;
					}
					}
				}
				window.clear();
				// Draw the text to the window
				window.draw(*text);
				window.display();
			}
		}

	}
}
