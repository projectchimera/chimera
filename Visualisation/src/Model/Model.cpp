/**
 * @file
 * Implementation of the Model class.
 */

#include "Model.h"

#include <iostream>
#include <fstream>
#include <limits>
#include <sstream>
#include <algorithm>

#include <stdio.h>
#include <dirent.h>

#include "../View/Views/LoadingView.h"

namespace Visualisation {
	namespace Model {

		Model::Model(int x, int y, std::string file) {
			currentstep = 0;
			updated = true;

			texture = "Resources/" + file + ".jpg";

			// Initiate x and y
			this->x = x;
			this->y = y;

			// Initiate min/max's on infinity/-infinity respectively
			minlong = std::numeric_limits<double>::infinity();
			maxlong = -std::numeric_limits<double>::infinity();
			minlat = std::numeric_limits<double>::infinity();
			maxlat = -std::numeric_limits<double>::infinity();

			// Calculate how many files to load
			struct dirent *de;
			DIR *dir = opendir("Resources/LatestRun");
			if(!dir){
				std::cerr << "Couldn't open file \"Resources/LatestRun\"." << std::endl;
				exit(-1);
			}
			steps = 0;
			while (de = readdir(dir)){
				++steps;
			}
			steps -= 3;
			closedir(dir);

			// Parse the _geo.csv-file
			std::string inifile = "Resources/pop_";
			inifile += file;
			inifile += "_geo.csv";
			parseFile(inifile);

			//Sort households on id
			std::sort(households.begin(),households.end(),Household::PointerCompare());

			//Read the starting situation (infected in the households)
			readStepFile("Resources/LatestRun/start.csv");
		}

		Model::~Model(){

		}

		void Model::notify() {
			updated = true;
		}

		void Model::removeEmptyClusters(){
			// Remove all empty cells
			for (unsigned int i = 0; i < clusters.size(); i++){
				if (clusters[i]->getSize() == 0){
					delete removeCluster(clusters[i]);
					i--;
				}
			}

			// Sort cells by size
			std::sort(clusters.begin(),clusters.end(),Cluster::PointerCompare());
			setSizes(clusters[0]->getSize(),std::max(x,y));
		}

		Cluster* Model::removeCluster(Cluster* c){
			auto it = std::find(clusters.begin(), clusters.end(), c);
			if (it != clusters.end()){
				clusters.erase(it);
			}
			return c;
		}

		void Model::readStepFile(std::string file) {
			std::ifstream f(file);
			if (!f.is_open()){
				return;
			}
			while (f){
				std::string s;
				if(!std::getline(f,s)){
					break;
				}
				std::istringstream ss(s);
				std::string hh_id;
				std::string infected;
				if(!std::getline(ss,hh_id,',')){
					break;
				}
				// If line doesn't contain data (first line of file), skip it
				if(hh_id.compare("\"hh_id\"") == 0){
					continue;
				}
				if(!std::getline(ss,infected,',')){
					break;
				}
				int id = std::stoi(hh_id);
				int inf = std::stoi(infected);
				// Add number of infected in the corresponding household to the vector
				households[id-1]->setInfected(inf);
			}
			currentstep++;
			updated = true;
		}

		void Model::parseFile(std::string file){
			std::ifstream f(file);
			while (f){
				std::string s;
				if(!std::getline (f,s)) {
					break;
				}
				std::istringstream ss(s);
				std::string hh_id;
				std::string lat;
				std::string lon;
				std::string size;
				if(!std::getline(ss,hh_id,',')){
					break;
				}
				if (hh_id.compare("\"hh_id\"") == 0){
					continue;
				}
				if(!std::getline(ss,lat,',')){
					break;
				}
				if(!std::getline(ss,lon,',')){
					break;
				}
				if(!std::getline(ss,size,',')){
					break;
				}
				int id = std::stoi(hh_id);

				// Calculate min and max latitude
				double latitude = std::stod(lat);
				if (latitude > maxlat) {
					maxlat = latitude;
				}
				if (latitude < minlat) {
					minlat = latitude;
				}

				// Calculate min and max longitude
				double longitude = std::stod(lon);
				if (longitude > maxlong) {
					maxlong = longitude;
				}
				if (longitude < minlong) {
					minlong = longitude;
				}

				int sizeint = stoi(size);

				// Create and add household
				Household* h = new Household(id,longitude,latitude,sizeint);
				households.push_back(h);
			}
		}

		void Model::generateClusters(){
			for (int i = 0; i < x; i++){
				for (int j = 0; j < y; j++){
					Cluster* c = new Cluster(i,j,x,y);
					c->subscribe(this);
					clusters.emplace_back(c);
				}
			}
		}

		void Model::addHouseholds(){
			double longdif = maxlong - minlong;
			double latdif = maxlat - minlat;
			// Logic to add households to the correct cluster
			for (unsigned int i = 0; i < households.size(); i++){
				bool added = false;
				for (int j = 0; j < x; j++){
					if (added == true){
						break;
					}
					if (households[i]->getLong() <= minlong + (j+1) * (longdif / double(x))){
						for (int k = 0; k < y; k++){
							if (added == true){
								break;
							}
							if (households[i]->getLat() <= minlat + (k+1) * (latdif / double(y))){
								clusters[j*y + k]->addHousehold(households[i]);
								added = true;
							}
						}
					}
				}
			}
		}

		void Model::setSizes(int biggest, int used){
			for (unsigned int i = 0; i < clusters.size(); i++){
				clusters[i]->setSize(biggest, used);
			}
		}

		void Model::zoomChanged(){
			updated = true;
		}

		void Model::setInfected(int inf){
			for (unsigned int i = 0; i < clusters.size(); i++){
				clusters[i]->setInfected(inf);
			}
		}

	}
}

