#ifndef MODEL_MODEL_H_
#define MODEL_MODEL_H_

/**
 * @file
 * Header for the Model class.
 */

#include <vector>
#include "../Subject.h"
#include "../Observer.h"
#include "Cluster.h"
#include "Household.h"
#include <string>

namespace Visualisation {
	namespace Model {

		class Model : public Subject , public Observer {
		public:
			/**
			 * Constructor for the model class
			 * @param x				Number of clusters on the x-axis
			 * @param y				Number of clusters on the y-axis
			 * @param file			The _geo.csv file to read as basis
			 */
			Model(int x, int y, std::string file);

			/// Default destructor
			virtual ~Model();

			/// Basic getters
			std::vector<Cluster*> getClusters() const{
				return clusters;
			}
			int getSteps() const{
				return steps;
			}
			std::string getTexture() const{
				return texture;
			}
			int getCurrentStep() const{
				return currentstep;
			}

			/// Implementation of Observer::notify()
			virtual void notify();

			/**
			 * Read a step file to update the clusters with
			 * @param file			The file to read
			 */
			void readStepFile(std::string file);

			/**
			 * Sets the updated-variable to true if the zoomlvl
			 * of the visualisation is changed
			 */
			void zoomChanged();

			/**
			 * Sets the current infected-amount and changes color accordingly
			 * @param inf			The step currently being drawn
			 */
			void setInfected(int inf);

			/// Generate the clusters
			void generateClusters();
			/// Add all households to the correct clusters
			void addHouseholds();
			/// Remove empty clusters
			void removeEmptyClusters();

		protected:
			/**
			 * Parses the _geo.csv-file
			 * @param file			The file to be parsed
			 */
			void parseFile(std::string file);

			/**
			 * Sets the sizes of all clusters
			 * @param biggest		Size of the biggest cluster
			 * @param used			Max(x,y)
			 */
			void setSizes(int biggest, int used);

			/**
			 * Remove a cluster from the container
			 * @param c				Pointer to the cluster to be removed
			 * @return Cluster*		Pointer to the removed cluster
			 */
			Cluster* removeCluster(Cluster* c);

			std::vector<Household*> households;			///< Container holding pointers to all households in the visualisation
			std::vector<Cluster*> clusters;				///< Container holding pointers to all clusters in the visualisation

			int x;										///< Number of clusters on the x-axis
			int y;										///< Number of clusters on the y-axis

			double minlong;								///< Minimum longitude of all households
			double maxlong;								///< Maximum longitude of all households
			double minlat;								///< Minimum latitude of all households
			double maxlat;								///< Maximum latitude of all households

			int steps;									///< Number of steps in the visualisation
			int currentstep;							///< Number of the current step being loaded
			std::string texture;						///< Filename of the background texture to be used

		};



	}
}



#endif /* MODEL_MODEL_H_ */
