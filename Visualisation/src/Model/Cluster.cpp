/**
 * @file
 * Implementation of the Cluster class.
 */

#include "Cluster.h"
#include "../VisParser.h"

namespace Visualisation {
	namespace Model {

		Cluster::Cluster(int xloc, int yloc, int x, int y) {
			this->x = xloc;
			this->y = yloc;

			updated = true;

			size = 0;
			infected = 0;
			currentTimeStep = 0;
			shape.setPosition(sf::Vector2f(float(xloc)*1000.f/float(x)+500.f/float(x),float(yloc)*1000.f/float(y)+500.f/float(y)));

			shape.setFillColor(sf::Color::Green);
		}

		Cluster::~Cluster() {

		}

		void Cluster::setColor(){
			int inf = 0;
			for (unsigned int i = 0; i < households.size(); i++){
				inf += households[i]->getInfected(infected);
			}

			// Set color according to infected rate
			VisParser* visparser = &VisParser::getInstance();
			float mod = 10.f * float(inf) / float(size);
			float redmod = mod * (visparser->getInfectedcolor().r - visparser->getHealthycolor().r);
			float red = visparser->getHealthycolor().r + redmod;
			if (visparser->getInfectedcolor().r > visparser->getHealthycolor().r){
				if (red > visparser->getInfectedcolor().r){
					red = visparser->getInfectedcolor().r;
				}
			}
			else {
				if (red < visparser->getInfectedcolor().r){
					red = visparser->getInfectedcolor().r;
				}
			}
			float greenmod = mod * (visparser->getInfectedcolor().g - visparser->getHealthycolor().g);
			float green = visparser->getHealthycolor().g + greenmod;
			if (visparser->getInfectedcolor().g > visparser->getHealthycolor().g){
				if (green > visparser->getInfectedcolor().g){
					green = visparser->getInfectedcolor().g;
				}
			}
			else {
				if (green < visparser->getInfectedcolor().g){
					green = visparser->getInfectedcolor().g;
				}
			}
			float bluemod = mod * (visparser->getInfectedcolor().b - visparser->getHealthycolor().b);
			float blue = visparser->getHealthycolor().b + bluemod;
			if (visparser->getInfectedcolor().b > visparser->getHealthycolor().b){
				if (blue > visparser->getInfectedcolor().b){
					blue = visparser->getInfectedcolor().b;
				}
			}
			else {
				if (blue < visparser->getInfectedcolor().b){
					blue = visparser->getInfectedcolor().b;
				}
			}

			shape.setFillColor(sf::Color(red,green,blue));
		}

		void Cluster::addHousehold(Household* h){
			households.push_back(h);
			this->size += h->getSize();
			updated = true;
		}

		void Cluster::setSize(int biggest, int used){
			// Calculate base size
			float base = 125.f/float(used);
			// Calculate extra size according to the biggest cluster used in the visualisation
			float extra = float(size)/float(biggest)*500.f/float(used);
			//set size of the cluster
			shape.setRadius(base + extra);
			// Recalculate position according to new size
			shape.setPosition(shape.getPosition().x - shape.getRadius(),shape.getPosition().y - shape.getRadius());
			updated = true;
		}

		void Cluster::setInfected(int inf){
			infected = inf;
			setColor();
			updated = true;
		}

		std::vector<int> Cluster::getInfected(int size) const{
			std::vector<int> inf;
			for (int i = 0; i < size; i++){
				int adder = 0;
				for (unsigned int j = 0; j < households.size(); j++){
					adder += households[j]->getInfected(i);
				}
				inf.push_back(adder);
			}
			return inf;
		}

		void Cluster::incTimeStep() {
			this->currentTimeStep++;
			updated = true;
		}

		void Cluster::decTimeStep() {
			this->currentTimeStep--;
			updated = true;
		}

		void Cluster::resetTimeStep() {
			this->currentTimeStep = 0;
			updated = true;
		}

		void Cluster::update() {
			updated = true;
		}

		bool Cluster::operator <(const Cluster& c) const{
			if (this->size < c.size){
				return true;
			}
			else {
				return false;
			}
		}

		bool Cluster::operator >(const Cluster& c) const{
			return (c<*this);
		}

		bool Cluster::operator <=(const Cluster& c) const{
			return !(*this>c);
		}

		bool Cluster::operator >=(const Cluster& c) const{
			return !(*this<c);
		}

		bool Cluster::operator ==(const Cluster& c) const{
			if (this->size == c.size){
				return true;
			}
			else {
				return false;
			}
		}

		bool Cluster::operator !=(const Cluster& c) const{
			return !(*this==c);
		}
	}
}
