#ifndef CLUSTER_H_
#define CLUSTER_H_

/**
 * @file
 * Header for the Cluster class.
 *
 * !warning!
 * Clusters in the visualisation are not the same as clusters in stride.
 * Here, clusters refer to cells in the visualisation, rather than
 * clusters as defined in stride (see stride code for more information)
 * !warning!
 */

#include <SFML/Graphics.hpp>
#include <vector>

#include "../Subject.h"
#include "Household.h"

namespace Visualisation {
	namespace Model {

		class Cluster : public Subject {
		public:
			/// Struct to compare 2 Clusterpointers
			struct PointerCompare {
				bool operator()(const Cluster* l, const Cluster* r) {
					return *l > *r;
				}
			};

			/**
			 * Constructor for the Cluster class
			 * @param xloc		The x-coördinate for the cluster
			 * @param yloc		The y-coördinate for the cluster
			 * @param x			The maximum of all x-coördinates
			 * @param y			The maximum of all y-coördinates
			 */
			Cluster(int xloc, int yloc, int x, int y);

			/// Default destructor
			virtual ~Cluster();

			/// Basic getters
			sf::CircleShape getShape() const{
				return shape;
			}
			int getSize() const{
				return size;
			}
			int getX() const {
				return x;
			}
			int getY() const {
				return y;
			}
			int getCurrentTimeStep() const {
				return currentTimeStep;
			}

			/**
			 * Adds a household to the cluster
			 * @param h			The household to be added
			 */
			void addHousehold(Household* h);

			 ///Sets the color of the cluster according to the infection rate
			void setColor();

			/**
			 * Sets the size of the cluster
			 * @param biggest	The size of the biggest cluster to compare with
			 * @param used		Max(x,y);
			 */
			void setSize(int biggest, int used);

			/**
			 * Sets current step and sets color accordingly
			 * @param inf			The step currently being drawn
			 */
			void setInfected(int inf);

			/**
			 * Calculates and returns for every step the number of infected
			 * @param size			The amount of steps in the visualisation
			 * @return				Vector with number of infected
			 */
			std::vector<int> getInfected(int size) const;

			/// Increases the current timestep by 1
			void incTimeStep();

			/// Decreses the current timestep by 1
			void decTimeStep();

			/// Resets the current timestep to 0
			void resetTimeStep();

			/// Sets updated to true
			void update();

			/// Operator overloading for sorting purposes
			bool operator <(const Cluster& c) const;
			bool operator >(const Cluster& c) const;
			bool operator <=(const Cluster& c) const;
			bool operator >=(const Cluster& c) const;
			bool operator ==(const Cluster& c) const;
			bool operator !=(const Cluster& c) const;

		protected:
			std::vector<Household*> households;		///< Container holding pointers to the households belonging to this cluster
			int size;								///< Size of the cluster
			int infected;							///< Number of infected people in the cluster
			sf::CircleShape shape;					///< Object to draw on screen

			int x;									///< X-location of the cluster
			int y;									///< Y-location of the cluster

			int currentTimeStep	;					///< The current time step in graphview
		};

	}
}

#endif /* CLUSTER_H_ */
