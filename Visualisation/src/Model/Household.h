#ifndef MODEL_HOUSEHOLD_H_
#define MODEL_HOUSEHOLD_H_

/**
 * @file
 * Header for the Household class.
 */

#include <vector>

namespace Visualisation {
	namespace Model {

		class Household{
		public:
			/// Struct to compare 2 Householdpointers
			struct PointerCompare {
				bool operator()(const Household* l, const Household* r) {
					return *l < *r;
				}
			};

			/**
			 *  Constructor for the household class
			 * @param id			The id of the household
			 * @param longitude		The longitude of the household
			 * @param latitude		The latitude of the household
			 * @param size			The size of the household
			 * @param infected		The number of infected in the household
			 */
			Household(int id, double longitude, double latitude, int size);

			/// Default destructor
			virtual ~Household();

			/**
			 * Adds a new step to the infected-values
			 * @param inf			The new value to be added
			 */
			void setInfected(int inf);

			/// Basic getters
			double getLong() const{
				return longitude;
			}
			double getLat() const{
				return latitude;
			}
			int getSize() const{
				return size;
			}
			int getInfected(int id) const{
				return infected[id];
			}
			int getId() const{
				return id;
			}

			/// Operator overloading for sorting purposes
			bool operator <(const Household& h) const;
			bool operator >(const Household& h) const;
			bool operator <=(const Household& h) const;
			bool operator >=(const Household& h) const;
			bool operator ==(const Household& h) const;
			bool operator !=(const Household& h) const;

		private:
			int 	id;							///< Id of the household
			double 	longitude;					///< Longitude of the household
			double 	latitude;					///< Latitude of the household
			int 	size;						///< Size of the household
			std::vector<int> 	infected;		///< Number of infected in the household
		};

	} /* namespace Model */
} /* namespace Visualisation */

#endif /* MODEL_HOUSEHOLD_H_ */
