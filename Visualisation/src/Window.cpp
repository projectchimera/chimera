/**
 * @file
 * Implementation of the Window class.
 */

#include "Window.h"

namespace Visualisation {

	sf::RenderWindow& Window::getInstance() {
		static sf::RenderWindow window;
		window.setFramerateLimit(60);
		window.setMouseCursorVisible(true);
		return window;
	}

}


