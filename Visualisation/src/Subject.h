#ifndef SUBJECT_H_
#define SUBJECT_H_

/**
 * @file
 * Header for the Model class.
 */

#include <vector>
#include "Observer.h"

namespace Visualisation {

	class Subject {

	public:
		/// pure virtual destructor
		virtual ~Subject() = 0;

		/**
		 * Subscribes the subject to an observer
		 * @param obs			The observer to subscribe to
		 */
		void subscribe(Observer* obs) const;
		/// Notifies all of it's observers if it's updated
		void notify();

		/**
		 * Removes an observer from the list
		 * @param obs			Pointer to the observer to be removed
		 */
		void removeObs(Observer* obs);

	protected:
		mutable std::vector<Observer*> observers;			///< Container holding pointers to the observers it's subscribed to
		bool updated;										///< Holds wether the subject is updated or not
	};
}



#endif /* SUBJECT_H_ */
