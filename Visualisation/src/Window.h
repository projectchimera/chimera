#ifndef WINDOW_H_
#define WINDOW_H_

/**
 * @file
 * Header for the Window class.
 */

#include <SFML/Graphics.hpp>

namespace Visualisation {

	class Window {

		/// Implemented as singleton

	public:
		static sf::RenderWindow& getInstance();

	private:
		Window(){};
		Window(Window const&);
		void operator=(Window const&);

	};

}



#endif /* WINDOW_H_ */
