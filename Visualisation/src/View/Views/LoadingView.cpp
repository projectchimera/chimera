/**
 * @file
 * Implementation of the LoadingView class.
 */

#include "LoadingView.h"

namespace Visualisation {
	namespace View {

		LoadingView::LoadingView() : View() {

		}

		LoadingView::~LoadingView() {

		}

		void LoadingView::draw(){
			window->clear();

			// Create the loading text
			sf::Text* loading = new sf::Text();
			loading->setString("Loading, please wait...");
			loading->setFont(font);
			loading->setColor(visparser->getTextcolor());
			loading->setCharacterSize(50);
			sf::FloatRect textRect = loading->getLocalBounds();
			loading->setOrigin(textRect.left + textRect.width / 2.f, textRect.top + textRect.height / 2.f);
			loading->setPosition(500, 333);

			// Create the currently loading text
			std::string text = "Currently loading file ";
			text += std::to_string(model->getCurrentStep());
			text += " out of ";
			text += std::to_string(model->getSteps());
			sf::Text* t = new sf::Text();
			t->setString(text);
			t->setFont(font);
			t->setColor(visparser->getTextcolor());
			t->setCharacterSize(50);
			sf::FloatRect textRect2 = t->getLocalBounds();
			t->setOrigin(textRect2.left + textRect2.width / 2.f, textRect2.top + textRect2.height / 2.f);
			t->setPosition(500, 666);

			window->draw(*loading);
			window->draw(*t);

			delete loading;
			delete t;

			window->display();
		}

		void LoadingView::setModel(const Model::Model* m){
			this->model = m;
			m->subscribe(this);
		}

	} /* namespace View */
} /* namespace Visualisation */
