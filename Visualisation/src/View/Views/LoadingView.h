#ifndef VIEW_LOADINGVIEW_H_
#define VIEW_LOADINGVIEW_H_

/**
 * @file
 * Header for the LoadingView class.
 */

#include "../View.h"
#include "../../Model/Model.h"
#include "../../VisParser.h"

namespace Visualisation {
	namespace View {

		class LoadingView : public View {
		public:
			/// Default constructor
			LoadingView();
			/// Default destructor
			virtual ~LoadingView();

			/**
			 * Draw the model to the window
			 * Implementation of View::draw()
			 */
			virtual void draw();

			/**
			 * Sets the model for the view
			 * Also loads the background texture
			 * @param m				Pointer to the model to be used
			 */
			void setModel(const Model::Model* m);

		protected:
			const Model::Model* model;				///< Pointer to the model
		};

	} /* namespace View */
} /* namespace Visualisation */

#endif /* VIEW_LOADINGVIEW_H_ */
