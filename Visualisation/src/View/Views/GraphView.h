#ifndef VIEW_GRAPHVIEW_H_
#define VIEW_GRAPHVIEW_H_

/**
 * @file
 * Header for the GraphView class.
 */

#include "../View.h"
#include "../../Model/Cluster.h"

namespace Visualisation {
	namespace View {

		class GraphView : public View {
		public:
			/**
			 * Constructor for the GraphView class
			 * @param size			Number of steps in the visualisation
			 */
			GraphView(int size);
			/// Default destructor
			virtual ~GraphView();

			/**
			 * Draw the model to the window
			 * Implementation of View::draw()
			 */
			virtual void draw();

			/**
			 * Sets the model for the view
			 * @param c				Pointer to the cluster to be used
			 */
			void setCluster(const Model::Cluster* c);

		protected:
			const Model::Cluster* cluster;			///< Pointer to the cluster

			int size;								///< Number of steps in the visualisation
		};

	} /* namespace View */
} /* namespace Visualisation */

#endif /* VIEW_GRAPHVIEW_H_ */
