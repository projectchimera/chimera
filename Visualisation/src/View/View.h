#ifndef VIEW_H_
#define VIEW_H_

/**
 * @file
 * Header for the View class.
 */

#include <SFML/Graphics.hpp>
#include "../Observer.h"
#include "../Window.h"
#include "../VisParser.h"

namespace Visualisation {
	namespace View {

		class View : public Observer {

		public:
			/// Default constructor for the View class
			View();
			/// Default destructor for the View class
			virtual ~View();

			/// Pure virtual draw-method
			virtual void draw() = 0;
			/// Implementation of Observer::notify()
			virtual void notify();

		protected:
			sf::RenderWindow* window;				///< Pointer to window

			VisParser* visparser;					///< The visparser used to load text color

			sf::Font font;							///< The font to be used

		};

	} /* namespace View */
} /* namespace Visualisation */

#endif /* VIEW_H_ */
