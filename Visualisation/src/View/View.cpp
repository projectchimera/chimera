/**
 * @file
 * Implementation of the View class.
 */

#include "View.h"

namespace Visualisation {
	namespace View {

		View::View() : Observer() {
			// Set the window
			window = &Window::getInstance();
			// Load the font
			font.loadFromFile("Resources/ADayinSeptember.otf");
			// Set the parser
			visparser = &VisParser::getInstance();
		}

		View::~View() {

		}

		void View::notify() {
			// When model is updated, draw it to the window
			draw();
		}

	} /* namespace View */
} /* namespace Visualisation */
